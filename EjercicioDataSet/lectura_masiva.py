'''
Programa que lee un grupo de imagenes de un directorio externo, las transforma en
escala de grises, en binaria y tambien divide cada imagen en sus canales individuales R, G, B;
los resultados se guardan en distintos directorios.
'''

# importar librería OpenCV
import cv2 as cv
# Funciones de Sistema Operativo
import os

initSequence=1 # inicio secuencia
numSequences= 8 # fin secuencia
cont_frame = 0 # contador auxiliar para implementar

#Directorio de lectura
path_RGB = "../DATASETS/17flowers/RGB"

# Directorios de escritura
# Imágenes GRAY
path_GRAY = "./Output-Gray"
# Imágenes BINARIAS (B/N)
path_Binary = "./Output-Binary"
# Canal R (RED)
path_R = "./channel-RED"
# Canal G (GREEN)
path_G = "./channel-GREEN"
# Canal B (BLUE)
path_B = "./channel-BLUE"

totalImages = int(len(os.listdir(path_RGB))) # Contabiliza número de archivos
# Bucle recorre desde el inicio a fin en un rango
cont_frame = 0 # contador auxiliar para implementar
for ns in range(initSequence,numSequences+1):
    cont_frame = cont_frame + 1
    dirimages = path_RGB + '/image_' + str(ns).zfill(4) + '.jpg'
    img = cv.imread(dirimages)
    # Transformar a ESCALA DE GRISES la imagen en color
    gray=cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    # la siguiente linea convierte la imagen en escala de grises a binaria
    (thresh, im_bw) = cv.threshold(gray, 128, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)

    # Separa canales RGB
    B = cv.extractChannel(img, 0)
    G = cv.extractChannel(img, 1)
    R = cv.extractChannel(img, 2)
    #otra manera de separar canales RGB
    #(B, G, R) = cv.split(img)

    # RGB - Green
    cv.imshow("G-RGB", G)# muestra la imagen en su canal verde

    # RGB - Red
    cv.imshow("R-RGB", R)# muestra la imagen en su canal rojo

    # RGB - Red
    cv.imshow("B-RGB", B)# muestra la imagen en su canal azul

    #guarda las imagenes en los directorios correspondientes con el formato especificado
    cv.imwrite(path_GRAY + '/grayimage_' + str(ns).zfill(4) + '.jpg', gray)
    cv.imwrite(path_Binary + '/binaryimage_' + str(ns).zfill(4) + '.jpg', im_bw)
    cv.imwrite(path_R + '/channelRimage_' + str(ns).zfill(4) + '.jpg', R)
    cv.imwrite(path_G + '/channelGimage_' + str(ns).zfill(4) + '.jpg', G)
    cv.imwrite(path_B + '/channelBRimage_' + str(ns).zfill(4) + '.jpg', B)
    print('/grayimage_' + str(ns).zfill(4) + '.jpg', gray)
cv.waitKey(0)#espera presionar una tecla para que la ventana desaparezca
cv.destroyAllWindows()#Cierra la ventana

print(dirimages)
print(cont_frame)
print(totalImages)