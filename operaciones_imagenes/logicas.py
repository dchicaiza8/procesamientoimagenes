# Usa OpenCV para realizar operaciones logicas entre imágenes
# cv.bitwise_and
#cv.bitwise_or
#cv. bitwise_not
#cv.bitwise_xor

#importamos libreria OpenCV
import cv2 as cv

# Leer imagen circulo
img1=cv.imread('./circulo.jpeg')# lee imagen circulo.jpeg con opencv
# Leer imagen rectangulo
img2=cv.imread('./rectangulo.jpeg')#lee imagen rectangulo.jpeg con opencv
#Redimensiona las imagenes para poder realizar operaciones entre ellas
imagen1_res = cv.resize(img1, dsize=(512, 512), interpolation=cv.INTER_CUBIC)
imagen2_res = cv.resize(img2, dsize=(512, 512), interpolation=cv.INTER_CUBIC)
#imprime las dimensiones las imagenes
print(imagen1_res.shape)
print(imagen2_res.shape)
# Realizar operaciones solicitadas
#operacion logica AND
res = cv.bitwise_and(imagen1_res,imagen2_res)
#operacion logica OR
#res = cv.bitwise_or(imagen1_res,imagen2_res)
#operacion logica NOT
#res = cv. bitwise_not(imagen1_res)
#operacion logica XOR
#res = cv.bitwise_xor(imagen1_res,imagen2_res)
#cv.imshow('imag1', img1)
#cv.imshow('imag2', img2)
cv.imshow('resultado', res)#muestra en pantalla el resultado de la operacion realizada
cv.waitKey(0)#espera presionar una tecla para que la ventana desaparezca