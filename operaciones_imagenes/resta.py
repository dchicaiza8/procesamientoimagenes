# Usar OpenCV para Sustracción  de imágenes
# cv.subtract

#importamos libreria OpenCV
import cv2 as cv

# Leer imagen mitad_mundo.jpg
img1=cv.imread('./mitad_mundo.jpg')
# Leer imagen Torre Eiffel
img2=cv.imread('./torre_eiffel.jpg')
#imprime dimensiones de las imagenes
print(img1.shape)
print(img2.shape)
#Redimensiona la imagen 1
imagen1_res = cv.resize(img1, dsize=(512, 512), interpolation=cv.INTER_CUBIC)
# Realizar operaciones solicitadas
resta = cv.subtract(imagen1_res,img2)
#muestra en pantalla la imagen 1
cv.imshow('imag1', img1)
#muestra en pantalla la imagen 2
cv.imshow('imag2', img2)
#muestra en pantalla el resultado de la operacion solicitada
cv.imshow('suma', resta)
cv.waitKey(0)#espera presionar una tecla para que la ventana desaparezca
cv.destroyAllWindows()#Cierra la ventana

