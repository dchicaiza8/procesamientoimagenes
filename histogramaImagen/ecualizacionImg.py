#Histograma y ecualizacion del histograma de una imagen en escala de grises
#Muestra la imagen original y la ecualizada con su respectivo histograma.
import cv2
from matplotlib import pyplot as plt

#img = cv2.imread('imagenes/estatua.png')
img = cv2.imread('imagenes/estrella.png')#lee la imagen con opencv
#img = cv2.imread('imagenes/lenagris.png')
#img = cv2.imread('imagenes/mapa.png')
#cv2.imshow('imag orig',img)
img_to_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)#convierte nuestra imagen al espacio de color YUV
#aplica el metodo de ecualizacion de histograma sobre el canal Y
img_to_yuv[:, :, 0] = cv2.equalizeHist(img_to_yuv[:, :, 0])
#convierte el canal Y a BGR
hist_equalization_result = cv2.cvtColor(img_to_yuv, cv2.COLOR_YUV2BGR)

#imagen original
hist1 = cv2.calcHist([img],[0],None,[256],[0,256])
#imagen ecualizada
hist2 = cv2.calcHist([hist_equalization_result],[0],None,[256],[0,256])
#mostrar imagen original
plt.subplot(221), plt.imshow(img, 'brg')
#histograma de la imagen original
plt.subplot(222), plt.plot(hist1)
#mostrar imagen ecualizada
plt.subplot(223), plt.imshow(hist_equalization_result,'brg_r')
#histograma de la imagen ecualizada
plt.subplot(224), plt.plot(hist2)
plt.xlim([0,256])
plt.show()
