#Programa que obtiene la imagen ecualizada de una imagen a color y la muestra en pantalla
import cv2

img = cv2.imread('./imagenes/lena1.png')# lee imagen con OpenCV
img_to_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)#convierte nuestra imagen al espacio de color YUV
#aplicamos el método de ecualización de histograma sobre el canal Y usando el método equalizeHist()
img_to_yuv[:, :, 0] = cv2.equalizeHist(img_to_yuv[:, :, 0])
#Convertimos el canal Y a RGB
hist_equalization_result = cv2.cvtColor(img_to_yuv, cv2.COLOR_YUV2BGR)

cv2.imshow('imagen original', img)
cv2.imshow('imagen ecualizada', hist_equalization_result)
cv2.waitKey(0)#espera presionar una tecla para que la ventana desaparezca
cv2.destroyAllWindows()#Cierra la ventana