#Grafica el histograma de una imagen en escala de grises

import cv2 as cv
from matplotlib import pyplot as plt

img = cv.imread('./imagenes/lenagris.png')#lee una imagen con matplotlib
plt.hist(img.ravel(),256,[0,256]);#funcion que obtiene el histograma de la imagen
plt.show()#muestra el histograma en pantalla


