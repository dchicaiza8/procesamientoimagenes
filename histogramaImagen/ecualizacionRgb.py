#Programa que obtiene la imagen ecualizada de una imagen a color rgb y muestra su histograma
#Muestra la imagen original y la ecualzada con su respectivo histograma.

import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img = cv.imread('./imagenes/lena1.png')#lee imagen con opencv
#cv2.split, divide la imagen en canales individuales b,g,r
b,g,r = cv.split(img)       # get b,g,r
rgb_img = cv.merge([r,g,b])# cv2.merge, fudiona canales individuales formando una imagen en color

img_to_yuv = cv.cvtColor(img, cv.COLOR_BGR2YUV)#convierte nuestra imagen al espacio de color YUV
#aplicamos el método de ecualización de histograma sobre el canal Y usando el método equalizeHist()
img_to_yuv[:, :, 0] = cv.equalizeHist(img_to_yuv[:, :, 0])
#Convertimos el canal Y a RGB
hist_equalization_result = cv.cvtColor(img_to_yuv, cv.COLOR_YUV2BGR)

cv.imwrite('./imagenes/resultimagen4.jpg', hist_equalization_result) #guarda la imagen ecualizada
resul=cv.imread('./imagenes/resultimagen4.jpg')#lee la imagen ecualizada
b,g,r = cv.split(resul)#divide la imagen ecualizada en canales individuales
rgb_resul = cv.merge([r,g,b])#une los canales individuales para obtener una imagen ecualizada a color

plt.subplot(221), plt.imshow(rgb_img, 'gray')#muestra en pantalla la imagen original
plt.subplot(222), plt.imshow(rgb_resul,'gray')#muestra en pantalla la imagen ecualizada

color = ('b','g','r')
for i,col in enumerate(color):
    #calcula el histograma de la imagen original
    histr = cv.calcHist([img],[i],None,[256],[0,256])
    plt.subplot(223)#dibuja los ejes coordenados
    plt.plot(histr,color = col)#dibuja en pantalla el histograma de la imagen original
    #obtiene el histograma de la imagen ecualizada
    histrecualizada = cv.calcHist([resul], [i], None, [256], [0, 256])
    #muestra en pantalla el histograma de la imagen ecualizada
    plt.subplot(224), plt.plot(histrecualizada, color=col)

plt.xlim([0,256])#rango en el eje x
plt.show()#funcion que muestra lo dibujado con pyplot