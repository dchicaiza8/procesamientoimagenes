#Programa que lee una imagen usando la libreria matplotlib

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

#leer imagen con matplotlib
img=mpimg.imread('./imagenes/lena.png')
imgplot = plt.imshow(img)#funcion que permite mostrar la imagen
plt.show()# Muestra la imagen en pantalla