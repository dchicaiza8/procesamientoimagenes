#Programa que lee una imagen usando la libreria OpenCV

#importamos libreria OpenCV
import cv2 as cv

imagen = cv.imread('./imagenes/lena.png')#leer imagen con libreria opencv
cv.imshow('Leer y mostrar imagen con opencv',imagen)#muestra imagen en pantalla con el titulo que esta entre comillas

cv.waitKey(0)#espera presionar una tecla para que la ventana desaparezca
cv.destroyAllWindows()#Cierra la ventana