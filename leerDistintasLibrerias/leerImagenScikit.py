#Programa que lee una imagen usando la libreria scikit-image

#imporamos la libreria necesaria
from skimage import io

#leer imagen con scikit-image
img = io.imread("./imagenes/lena.png")
io.imshow(img)#funcion que permite mostrar la imagen
io.show()# Muestra la imagen en pantalla